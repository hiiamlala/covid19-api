var statistic = require("./src/StatisticGetter");
var news = require("./src/NewsGetter");
var trend = require("./src/TrendsGetter");
const express = require('express');
const app = express();
const port = 1210;

app.get('/api/statistic', (req, res) => {
    let queryParams = req.query;
    if(queryParams.type === 'country'){
        res.json({
            "data": statistic.countries,
            "lastUpdate": statistic.lastUpdate
        });
    }
    else if(queryParams.type === 'province'){
        res.json({
            "data":statistic.provinces,
            "lastUpdate": statistic.lastUpdate
        })
    }
    else if(queryParams.type === 'world'){
        res.json({
            "confirmed": statistic.confirmed,
            "deaths": statistic.deaths,
            "recover": statistic.recovered,
            "lastUpdate": statistic.lastUpdate
        })
    }
    else{
        res.sendStatus(400);
    }
});

app.get('/api/news', (req, res) => {
    res.json({
        "data": news.topNews,
        "lastUpdate": news.lastUpdate
    });
});

app.get('/api/trends', (req, res)=>{
    res.json({
        "data": {
            "trends": trend.trends,
            "totalConfirmed": trend.totalConfirmed,
            "totalDeaths": trend.totalDeaths,
            "totalRecovered": trend.totalRecovered
        },
        "lastUpdate": trend.lastUpdate
    });
})

app.listen(port, () => console.log(`API listening on port ${port}!`))