const fetch = require('node-fetch');

const STATISTIC_TMP_FILE = "./tmp/statistic.json";
const INTERVAL = 5000;

if(!require('fs').existsSync('./tmp')) require('fs').mkdirSync('./tmp');

const baseUri = "https://corona-api.kompa.ai/graphql";
const fetchCountriesDataParam = {
    "credentials": "omit",
    "headers": {
        "accept": "*/*",
        "accept-language": "en-US,en;q=0.9,vi;q=0.8",
        "content-type": "application/json",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "origin": "https://corona.kompa.ai",
        "referrer": "https://corona.kompa.ai/",
        "referrerPolicy": "no-referrer-when-downgrade",
    },
    "body": `{"operationName":"countries","variables":{},"query":"query countries {\\n  globalCasesToday {\\n    country\\n    totalCase\\n    totalDeaths\\n    totalRecovered\\n    longitude\\n    latitude\\n    __typename\\n  }\\n  provinces {\\n    Province_Name\\n    Province_Id\\n    Lat\\n    Long\\n    Confirmed\\n    Deaths\\n    Recovered\\n    Last_Update\\n    __typename\\n  }\\n}\\n"}`,
    "method": "POST",
    "mode": "cors"
}

class StatisticData {
    countries;
    provinces;
    confirmed = 0;
    deaths = 0;
    recovered = 0;
    lastUpdate;
    loop;

    constructor(){
        this.start();
    }

    async fetch(){
        let fetchRt = await fetch(baseUri, fetchCountriesDataParam);
        let jsonData = await fetchRt.json();
        let provinces_raw = jsonData.data.provinces;
        let provinces = {};
        let countries_raw = jsonData.data.globalCasesToday;
        let countries = {};
        let confirmed = 0;
        let deaths = 0;
        let recovered = 0;
        countries_raw.forEach((country,index,arr)=>{
            if(country.country==='Total:'){
                console.log((country.totalCase||'0').split(',').join(''));
                confirmed = parseInt((country.totalCase||'0').split(',').join(''));
                deaths = parseInt((country.totalDeaths||'0').split(',').join(''));
                recovered = parseInt((country.totalRecovered||'0').split(',').join(''));
            }
            else{
                countries[country.country] = {
                    "lat": country.latitude,
                    "long": country.longitude,
                    "confirmed": parseInt((country.totalCase||'0').split(',').join('')),
                    "deaths": parseInt((country.totalDeaths||'0').split(',').join('')),
                    "recovered": parseInt((country.totalRecovered||'0').split(',').join(''))
                }
            }
        });

        provinces_raw.forEach((province,index,arr)=>{
            provinces[province.Province_Id] = {
                "name": province.Province_Name,
                "lat": province.Lat,
                "long": province.Long,
                "confirmed": parseInt((province.Confirmed||'0').split(',').join('')),
                "deaths": parseInt((province.Deaths||'0').split(',').join('')),
                "Recovered": parseInt((province.Recovered||'0').split(',').join('')),
                "lastUpdate": province.Last_Update 
            }
        })

        if(!this.countries || (this.countries['Vietnam'].confirmed !== countries['Vietnam'].confirmed || this.countries['Vietnam'].deaths !== countries['Vietnam'].deaths || this.countries['Vietnam'].recovered !== countries['Vietnam'].recovered)){
            this.provinces = provinces;
            if(this.countries)
            console.log(`[Vietnam's Data Change] Confirmed: ${this.countries['Vietnam'].confirmed}\t Deaths: ${this.countries['Vietnam'].deaths}\t Recovered: ${this.countries['Vietnam'].recovered}`);
        }

        if(this.confirmed !== confirmed || this.deaths !== deaths || this.recovered !== recovered){
            this.countries = countries;
            this.confirmed = confirmed;
            this.deaths = deaths;
            this.recovered = recovered;
            this.lastUpdate = new Date().getTime();
            console.log(`[Data change] Total confirmed: ${this.confirmed}\t Deaths: ${this.deaths}\t Recovered: ${this.recovered}`);
        }

        require('fs').writeFileSync(STATISTIC_TMP_FILE, JSON.stringify({
            "countries": this.countries,
            "provinces": this.provinces,
            "lastUpdate": this.lastUpdate
        }));
    }

    stop() {
        clearInterval(this.loop);
    }

    start() {
        this.fetch();
        this.loop = setInterval(this.fetch,INTERVAL);
    }
}

var statistic = new StatisticData();
module.exports = statistic;