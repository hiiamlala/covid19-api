const fetch = require('node-fetch');

const INTERVAL = 24*60*60*1000;

const baseUri = "https://corona-api.kompa.ai/graphql";
const fetchCountriesDataParam = {
    "credentials": "omit",
    "headers": {
        "accept": "*/*",
        "accept-language": "en-US,en;q=0.9,vi;q=0.8",
        "content-type": "application/json",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "origin": "https://corona.kompa.ai",
        "referrer": "https://corona.kompa.ai/",
        "referrerPolicy": "no-referrer-when-downgrade",
    }, 
    "body":`{"operationName":"totalConfirmed","variables":{},"query":"query totalConfirmed {\\n  totalConfirmed\\n  totalConfirmedLast\\n totalDeaths \\n totalDeathsLast \\n totalRecovered \\n totalRecoveredLast \\n trendlineGlobalCases {\\n    date\\n    confirmed\\n    __typename\\n  }\\n trendlineGlobalCases {\\n    date\\n    death\\n    __typename\\n  } \\n  trendlineGlobalCases {\\n    date\\n    recovered\\n    __typename\\n  } \\n }\\n"}`,
    "method": "POST",
    "mode": "cors"
};

class TrendsGetter {
    trends = {};
    totalConfirmed = {};
    totalDeaths = {};
    totalRecovered = {};
    lastUpdate;
    loop;

    constructor(){
        this.start();
    }

    async fetch(){
        let fetchRt = await fetch(baseUri, fetchCountriesDataParam);
        let jsonData = await fetchRt.json();
        let trends = {};
        let totalConfirmed = {
            "present": parseInt(jsonData.data.totalConfirmed||0),
            "last": parseInt(jsonData.data.totalConfirmedLast||0)
        }
        let totalDeaths = {
            "present": parseInt(jsonData.data.totalDeaths||0),
            "last": parseInt(jsonData.data.totalDeathsLast||0)
        };
        let totalRecovered = {
            "present": parseInt(jsonData.data.totalRecovered||0),
            "last": parseInt(jsonData.data.totalRecoveredLast||0)
        };

        jsonData.data.trendlineGlobalCases.forEach((trend,index,arr)=>{
            trends[trend.date] = {
                "confirmed": parseInt(trend.confirmed||0),
                "deaths": parseInt(trend.death||0),
                "recovered": parseInt(trend.recovered||0)
            }
        });

        this.totalConfirmed = totalConfirmed;
        this.totalDeaths = totalDeaths;
        this.totalRecovered = totalRecovered;
        this.trends = trends;
        this.lastUpdate = new Date().getTime();
    }

    stop(){
        clearInterval(this.loop);
    }

    start(){
        this.fetch();
        this.loop = setInterval(this.fetch,INTERVAL);
    }
}

var trendsGetter = new TrendsGetter();
module.exports = trendsGetter;