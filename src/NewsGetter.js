const fetch = require('node-fetch');

const INTERVAL = 5000;

const baseUri = "https://corona-api.kompa.ai/graphql";
const fetchCountriesDataParam = {
    "credentials": "omit",
    "headers": {
        "accept": "*/*",
        "accept-language": "en-US,en;q=0.9,vi;q=0.8",
        "content-type": "application/json",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-site",
        "origin": "https://corona.kompa.ai",
        "referrer": "https://corona.kompa.ai/",
        "referrerPolicy": "no-referrer-when-downgrade",
    }, 
    "body":`{"operationName":"topTrueNews","variables":{},"query":"query topTrueNews {\\n  topTrueNews {\\n    id\\n    type\\n    title\\n    content\\n    url\\n    siteName\\n    publishedDate\\n    author\\n    picture\\n    __typename\\n  }\\n}\\n"}`,
    "method": "POST",
    "mode": "cors"
};

class NewsGetter {
    topNews = {};
    lastUpdate;
    loop;

    constructor(){
        this.start();
    }

    async fetch(){
        let fetchRt = await fetch(baseUri, fetchCountriesDataParam);
        let jsonData = await fetchRt.json();
        let topNews = {};
        jsonData.data.topTrueNews.forEach((news,index,arr)=>{
            topNews[news.id] = {
                "title": news.title,
                "content": news.content + "...",
                "url": news.url,
                "site": news.siteName,
                "publishedDate": news.publishedDate,
                "author": news.author,
                "picture": news.picture
            };
        });
        this.topNews = topNews;
        this.lastUpdate = new Date().getTime();
    }

    stop(){
        clearInterval(this.loop);
    }

    start(){
        this.fetch();
        this.loop = setInterval(this.fetch,INTERVAL);
    }
}

var news = new NewsGetter();
module.exports = news;